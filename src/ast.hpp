#ifndef AST_HPP
#define AST_HPP

#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/raw_ostream.h"

#include <string>
#include <cwchar>
#include <cctype>
#include <iostream>
#include <memory>
#include <utility>
#include <vector>

#include "lexer.hpp"

using llvm::Value;
using llvm::LLVMContext;
using llvm::IRBuilder;
using llvm::Module;
using llvm::ConstantFP;
using llvm::APFloat;
using llvm::Type;
using llvm::Function;
using llvm::FunctionType;
using llvm::BasicBlock;
using llvm::verifyFunction;
using llvm::errs;
using llvm::raw_ostream;

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::unique_ptr;
using std::move;
using std::vector;
using std::pair;

//===---------------------------------------------------------------------------------------
// Abstract Syntax Tree
//===---------------------------------------------------------------------------------------
raw_ostream& indent(raw_ostream& O, int size) {
    return O << string(size, ' ');
}

/// ExprAST - Base class for all expression nodes.
/// TODO: Add a type field for real
class ExprAST {
    SrcLoc Loc;
public:
    ExprAST(SrcLoc Loc = CurLoc) : Loc(Loc) {}
    virtual ~ExprAST() {}
    virtual Value* codegen() = 0;

    int getLn() const { return Loc.Ln; }
    int getCol() const { return Loc.Col; }
    virtual raw_ostream& dump(raw_ostream& out, int _) {
        return out << ':' << getLn() << ':' << getCol() << '\n';
    }
};

/// NumberExprAST - Expression class for numberic literals like "1.0".
class NumberExprAST : public ExprAST {
    double Val;
public:
    NumberExprAST(double Val) : Val(Val) {}
    raw_ostream& dump(raw_ostream& out, int ind) override {
        return ExprAST::dump(out << Val, ind);
    }
    Value* codegen() override;
};


/// VariableExprAST - Expression class for referencing a variable, like "a".
class VariableExprAST : public ExprAST {
    string Name;
public:
    VariableExprAST(SrcLoc Loc, const string &Name) : Name(Name) {}
    Value* codegen() override;
    raw_ostream& dump(raw_ostream& out, int ind) override {
        return ExprAST::dump(out << Name, ind);
    }
    const string &getName() const { return Name; }
};


/// BinaryExprAST - Expression class for a binary operator.
class BinaryExprAST : public ExprAST {
    char Op;
    unique_ptr<ExprAST> LHS, RHS;
public:
    BinaryExprAST(SrcLoc Loc, char op,
                  unique_ptr<ExprAST> LHS, unique_ptr<ExprAST> RHS) :
        ExprAST(Loc), Op(op), LHS(move(LHS)), RHS(move(RHS)) {}
    Value* codegen() override;
    raw_ostream& dump(raw_ostream& out, int ind) override {
        ExprAST::dump(out << "binary" << Op, ind);
        LHS->dump(indent(out, ind) << "LHS:", ind + 1);
        RHS->dump(indent(out, ind) << "RHS:", ind + 1);
        return out;
    }
};


/// CallExprAST - Expression class for function calls.
class CallExprAST : public ExprAST {
    string Callee;
    vector<unique_ptr<ExprAST>> Args;
public:
    CallExprAST(
        SrcLoc Loc,
        const string &Callee, vector<unique_ptr<ExprAST>> Args) :
        Callee(Callee), Args(move(Args)
    ) {}
    Value* codegen() override;
    raw_ostream& dump(raw_ostream& out, int ind) override {
        ExprAST::dump(out << "call " << Callee, ind);
        for (const auto &Arg : Args)
            Arg->dump(indent(out, ind + 1), ind + 1);
        return out;
    }
};


class IfExprAST : public ExprAST {
    unique_ptr<ExprAST> Cond, Then, Else;

public:
    IfExprAST(SrcLoc Loc,
        unique_ptr<ExprAST> Cond,
        unique_ptr<ExprAST> Then,
        unique_ptr<ExprAST> Else):
        ExprAST(Loc), Cond(move(Cond)), Then(move(Then)), Else(move(Else)) {}
    Value* codegen() override;
    raw_ostream& dump(raw_ostream& out, int ind) override {
        ExprAST::dump(out << "if", ind);
        Cond->dump(indent(out, ind) << "Cond:", ind + 1);
        Then->dump(indent(out, ind) << "Then:", ind + 1);
        Else->dump(indent(out, ind) << "Else:", ind + 1);
        return out;
    }
};

/// ForExprAST - Expression class for for/in.
class ForExprAST : public ExprAST {
    string VarName;
    unique_ptr<ExprAST> Start, End, Step, Body;

public:
    ForExprAST(
        string& VarName,
        unique_ptr<ExprAST> Start,
        unique_ptr<ExprAST> End,
        unique_ptr<ExprAST> Step,
        unique_ptr<ExprAST> Body):
    VarName(VarName),
    Start(move(Start)),
    End(move(End)),
    Step(move(Step)),
    Body(move(Body)) {}

    Value* codegen() override;

    raw_ostream& dump(raw_ostream& out, int ind) override {
        ExprAST::dump(out << "for", ind);
        Start->dump(indent(out, ind) << "Cond:", ind + 1);
        End->dump(indent(out, ind) << "End:", ind + 1);
        Step->dump(indent(out, ind) << "Step:", ind + 1);
        Body->dump(indent(out, ind) << "Body:", ind + 1);
        return out;
    }
};

/// UnaryExprAST - Expression class for a unary operator.
class UnaryExprAST : public ExprAST {
    char Opcode;
    unique_ptr<ExprAST> Operand;
public:
    UnaryExprAST(char Opcode, unique_ptr<ExprAST> Operand):
        Opcode(Opcode), Operand(move(Operand)) {}

    Value* codegen() override;

    raw_ostream& dump(raw_ostream& out, int ind) override {
        ExprAST::dump(out << "unary" << Opcode, ind);
        Operand->dump(out, ind + 1);
        return out;
    }
};

/// LetExprAST - Expression class for var
class LetExprAST : public ExprAST {
    vector<pair<string, unique_ptr<ExprAST>>> VarNames;
    unique_ptr<ExprAST> Body;

public:
    LetExprAST(vector<pair<string, unique_ptr<ExprAST>>> VarNames, unique_ptr<ExprAST> Body):
        VarNames(move(VarNames)), Body(move(Body)) {}

    Value* codegen() override;

    raw_ostream& dump(raw_ostream& out, int ind) override {
        ExprAST::dump(out << "var", ind);
        for (const auto &NamedVar : VarNames)
        NamedVar.second->dump(indent(out, ind) << NamedVar.first << ':', ind + 1);
        Body->dump(indent(out, ind) << "Body:", ind + 1);
        return out;
    }
};

/// PrototypeAST - This class represents the "prototype" for a function,
/// which captures its name, and its argument names (thus implicitly the number
/// of arguments the function takes).
class PrototypeAST {
    string Name;
    vector<string> Args;
    bool IsOperator;
    unsigned int Precedence;  // Precedence if a binary op.
    int Ln;

public:
    PrototypeAST(
        SrcLoc Loc,
        const string &Name, vector<string> Args,
        bool IsOperator = false, unsigned Prec = 0
    ) :
    Name(Name), Args(move(Args)), IsOperator(IsOperator), Precedence(Prec) {}

    const string& getName() const { return Name; }
    Function* codegen();

    bool isUnaryOp() const { return IsOperator && Args.size() == 1; }
    bool isBinaryOp() const { return IsOperator && Args.size() == 2; }

    char getOperatorName() const {
        assert(isUnaryOp() || isBinaryOp());
        return Name[Name.size() - 1];
    }

    unsigned getBinaryPrecedence() const { return Precedence; }
    int getLn() const { return Ln; }
};

static map<string, unique_ptr<PrototypeAST>> FunctionProtos;

/// FunctionAST - This class represents a function definition itself.
class FunctionAST {
    unique_ptr<PrototypeAST> Proto;
    unique_ptr<ExprAST> Body;

public:
    FunctionAST(unique_ptr<PrototypeAST> Proto, unique_ptr<ExprAST> Body) :
        Proto(move(Proto)), Body(move(Body)) {}
    Function* codegen();
    raw_ostream& dump(raw_ostream& out, int ind) {
        indent(out, ind) << "FunctionAST\n";
        ++ind;
        indent(out, ind) << "Body:";
        return Body ? Body->dump(out, ind) : out << "null\n";
    }
};

#endif