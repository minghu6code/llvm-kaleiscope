#ifndef GLOBALS_HPP
#define GLOBALS_HPP

#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/DIBuilder.h"
#include "llvm/Support/Error.h"
#include <string>
#include <iostream>
#include <utility>
#include <vector>
#include <map>

#include "jit.hpp"

using llvm::Value;
using llvm::LLVMContext;
using llvm::IRBuilder;
using llvm::Module;
using llvm::AllocaInst;
using llvm::ExitOnError;
using llvm::DIBuilder;


using std::string;
using std::unique_ptr;
using std::make_unique;
using std::move;
using std::vector;
using std::map;

using llvm::orc::KaleidoscopeJIT;

static unique_ptr<LLVMContext> TheContext;
static unique_ptr<Module> TheModule;
static unique_ptr<IRBuilder<>> Builder;
static ExitOnError ExitOnErr;
static map<string, AllocaInst*> NamedValues;
static unique_ptr<KaleidoscopeJIT> TheJIT;
static unique_ptr<DIBuilder> DBuilder;

#endif