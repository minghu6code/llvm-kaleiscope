#ifndef DBI_HPP
#define DBI_HPP

#include "llvm/IR/DebugInfoMetadata.h"
#include "llvm/IR/DIBuilder.h"

#include "ast.hpp"

#include <map>
#include <string>
#include <vector>

#include "globals.hpp"
#include "ast.hpp"

using std::vector;
using std::string;

using llvm::DICompileUnit;
using llvm::DIType;
using llvm::DIScope;
using llvm::DIBuilder;
using llvm::DebugLoc;
using llvm::DILocation;
using llvm::DISubroutineType;
using llvm::DIFile;
using llvm::Metadata;
using llvm::SmallVector;

using llvm::dwarf::DW_ATE_float;

struct DebugInfo {
    DICompileUnit* TheCU;
    DIType* DblTy;
    vector<DIScope*> LexBlocks;

    void emitLoc(ExprAST* AST);
    DIType* getDoubleTy();
} KSDbgInfo;

DIType* DebugInfo::getDoubleTy() {
    if (DblTy)
        return DblTy;

    DblTy = DBuilder->createBasicType("double", 64, DW_ATE_float);
    return DblTy;
}

void DebugInfo::emitLoc(ExprAST* AST) {
    if (!AST)
        return Builder->SetCurrentDebugLocation(DebugLoc());
    DIScope* Scope;
    if (LexBlocks.empty())
        Scope = TheCU;
    else
        Scope = LexBlocks.back();
    Builder->SetCurrentDebugLocation(
        DILocation::get(
            Scope->getContext(),
            AST->getLn(),
            AST->getCol(),
            Scope
        )
    );
}

static DISubroutineType* CreateFunctionType(unsigned NumArgs, DIFile* Unit) {
    SmallVector<Metadata*, 8> EltTys;
    DIType* DblTy = KSDbgInfo.getDoubleTy();

    EltTys.push_back(DblTy);
    for (unsigned i = 0, e = NumArgs; i != e; ++i)
        EltTys.push_back(DblTy);

    return DBuilder->createSubroutineType(DBuilder->getOrCreateTypeArray(EltTys));
}

#endif