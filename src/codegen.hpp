#ifndef CODEGEN_HPP
#define CODEGEN_HPP

#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include <string>
#include <cwchar>
#include <cctype>
#include <iostream>
#include <memory>
#include <utility>
#include <vector>
#include <map>

#include "utils.hpp"
#include "ast.hpp"
#include "parser.hpp"
#include "jit.hpp"
#include "globals.hpp"
#include "dbi.hpp"

using llvm::Value;
using llvm::LLVMContext;
using llvm::IRBuilder;
using llvm::Module;
using llvm::ConstantFP;
using llvm::APFloat;
using llvm::Type;
using llvm::Function;
using llvm::FunctionType;
using llvm::BasicBlock;
using llvm::verifyFunction;
using llvm::errs;
using llvm::ExitOnError;
using llvm::PHINode;
using llvm::Constant;
using llvm::AllocaInst;
using llvm::StringRef;
using llvm::DIFile;
using llvm::DICompileUnit;
using llvm::DIType;
using llvm::DIScope;
using llvm::DIBuilder;
using llvm::DebugLoc;
using llvm::DILocation;
using llvm::DISubroutineType;
using llvm::DIFile;
using llvm::Metadata;
using llvm::SmallVector;
using llvm::DISubprogram;
using llvm::DINode;
using llvm::DILocalVariable;

using llvm::orc::KaleidoscopeJIT;

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::unique_ptr;
using std::make_unique;
using std::move;
using std::vector;
using std::map;


//===---------------------------------------------------------------------------------------
// Code generation
//===---------------------------------------------------------------------------------------

Value* LogErrorV(const char* Str) {
    LogError(Str);
    return nullptr;
}

static AllocaInst* CreateEntryBlockAlloca(Function* TheFunction, StringRef VarName) {
    IRBuilder<> TmpB(&TheFunction->getEntryBlock(), TheFunction->getEntryBlock().begin());

    return TmpB.CreateAlloca(Type::getDoubleTy(*TheContext), nullptr, VarName);
}

Function* getFunction(string Name) {
    // First, see if the function has already been added to the current module.
    if (Function* F = TheModule->getFunction(Name))
        return F;

    // If not, check whether we can codegen the declaration from some existing prototype.
    auto FI = FunctionProtos.find(Name);
    if (FI != FunctionProtos.end())
        return FI->second->codegen();

    // If no existing prototype exists, return null.
    return nullptr;
}

Value* NumberExprAST::codegen() {
    KSDbgInfo.emitLoc(this);
    return ConstantFP::get(*TheContext, APFloat(Val));
}

Value* VariableExprAST::codegen() {
    // Look this variable up in the function.
    Value* V = NamedValues[Name];
    if (!V)
        return LogErrorV("Unknown variable name");
    KSDbgInfo.emitLoc(this);
    // Load the value
    return Builder->CreateLoad(V, Name.c_str());
}

Value* BinaryExprAST::codegen() {
    KSDbgInfo.emitLoc(this);
    // Special case '=' because we don't want to emit the LHS as an expression.
    // mutable variable assignment
    if (Op == '=') {
        // LLVM builds without RTTI by default.
        // if build LLVM with RTTI
        // can be changed to a dynamic_cast for automatic error checking.
        auto LHSE = static_cast<VariableExprAST*>(LHS.get());
        if (!LHSE)
            return LogErrorV("destination of '=' must be a variable");
        // Codegen the RHS.
        auto Val = RHS->codegen();
        if (!Val)
            return nullptr;

        // Look up the name.
        auto Variable = NamedValues[LHSE->getName()];
        if (!Variable)
            return LogErrorV("Unknown variable name");

        Builder->CreateStore(Val, Variable);
        return Val;
    }

    Value* L = LHS->codegen();
    Value* R = RHS->codegen();
    if (!L || !R)
        return nullptr;
    switch (Op) {
        case '+':
            return Builder->CreateFAdd(L, R, "addtmp");
        case '-':
            return Builder->CreateFSub(L, R, "subtmp");
        case '*':
            return Builder->CreateFMul(L, R, "multmp");
        case '<':
            L = Builder->CreateFCmpULT(L, R, "cmptmp");
            // Convert bool 0/1 to double 0.0 or 1.0
            return Builder->CreateUIToFP(L, Type::getDoubleTy(*TheContext), "booltmp");
        default:
            break;
    }

    // If is wasn't a builtin binary operator, it must be a user defined one.
    // Emit a call to it.
    auto F = getFunction(string("binary") + Op);
    assert(F && "binary operator not found!");

    Value* Ops[2] = { L, R };
    return Builder->CreateCall(F, Ops, "binop");
}

Value* CallExprAST::codegen() {
    KSDbgInfo.emitLoc(this);

    // look up the name in the global module table.
    Function* CalleeF = getFunction(Callee);
    if (!CalleeF)
        return LogErrorV("Unknown function referenced");

    // If arguments mismatch error.
    if (CalleeF->arg_size() != Args.size())
        return LogErrorV("Incorrect # arguments passed");

    vector<Value*> ArgsV;
    for (unsigned i = 0, e = Args.size(); i != e; ++i) {
        ArgsV.push_back(Args[i]->codegen());
        if (!ArgsV.back())
            return nullptr;
    }

    return Builder->CreateCall(CalleeF, ArgsV, "calltmp");
}

Function* PrototypeAST::codegen() {
    vector<Type*> Doubles(Args.size(), Type::getDoubleTy(*TheContext));

    auto FT = FunctionType::get(Type::getDoubleTy(*TheContext), Doubles, false);
    auto F = Function::Create(FT, Function::ExternalLinkage, Name, TheModule.get());

    unsigned Idx = 0;
    // Set names for all arguments
    for (auto &Arg : F->args())
        Arg.setName(Args[Idx++]);

    return F;
}

Function* FunctionAST::codegen() {
    // Transfer ownership of the prototype to the FunctionProtos map,
    // but keep a reference to it for use below.
    auto &P = *Proto;
    FunctionProtos[Proto->getName()] = move(Proto);
    auto TheFunction = getFunction(P.getName());
    if (!TheFunction)
        return nullptr;

    if (P.isBinaryOp())
        BinopPrecedence[P.getOperatorName()] = P.getBinaryPrecedence();

    // Create a new basic block to start insertion into.
    BasicBlock* BB = BasicBlock::Create(*TheContext, "entry", TheFunction);
    Builder->SetInsertPoint(BB);

    DIFile* Unit = DBuilder->createFile(
        KSDbgInfo.TheCU->getFilename(),
        KSDbgInfo.TheCU->getDirectory()
    );
    DIScope* FContext = Unit;
    unsigned Ln = P.getLn();
    unsigned ScopeLn = Ln;
    DISubprogram* SP = DBuilder->createFunction(
        FContext, P.getName(), StringRef(), Unit, Ln,
        CreateFunctionType(TheFunction->arg_size(), Unit), ScopeLn,
        DINode::FlagPrototyped, DISubprogram::SPFlagDefinition
    );
    TheFunction->setSubprogram(SP);

    // Push the current scope.
    KSDbgInfo.LexBlocks.push_back(SP);

    // Unset the location for the prologue emission
    // (leading instructions with no location in a fucntion are considered
    // part of the prologue and the debugger will run past them when breaking on a function)
    KSDbgInfo.emitLoc(nullptr);

    // Record the function arguments in the NamedValues map.
    NamedValues.clear();
    unsigned ArgIdx = 0;
    for (auto &Arg : TheFunction->args()) {
        // Create an alloca for this variable.
        AllocaInst* Alloca = CreateEntryBlockAlloca(TheFunction, Arg.getName());

        // Create a debug descriptor for the variable.
        DILocalVariable *D = DBuilder->createParameterVariable(
            SP, Arg.getName(), ++ArgIdx, Unit, Ln, KSDbgInfo.getDoubleTy(),
            true
        );
        DBuilder->insertDeclare(
            Alloca, D, DBuilder->createExpression(),
            DILocation::get(SP->getContext(), Ln, 0, SP),
            Builder->GetInsertBlock()
        );

        // Store the initial value into the alloca.
        Builder->CreateStore(&Arg, Alloca);

        // Add arguments to variables symbol table.
        NamedValues[string(Arg.getName())] = Alloca;
    }

    KSDbgInfo.emitLoc(Body.get());

    if (Value* RetVal = Body->codegen()) {
        // Finish off the function.
        Builder->CreateRet(RetVal);

        // Pop off the lexical block for the function.
        KSDbgInfo.LexBlocks.pop_back();

        // Validate the generated code, checking for consistency.
        verifyFunction(*TheFunction);

        return TheFunction;
    }

    // Error reading body, remove function.
    TheFunction->eraseFromParent();

    if (P.isBinaryOp())
        BinopPrecedence.erase(Proto->getOperatorName());

    // Pop off the lexical block for the function since we added it unconditionally.
    KSDbgInfo.LexBlocks.pop_back();

    return nullptr;
}

Value* IfExprAST::codegen() {
    KSDbgInfo.emitLoc(this);

    Value* CondV = Cond->codegen();
    if (!CondV)
        return nullptr;

    // Convert condition to a bool comparing non-equal to 0.0.
    CondV = Builder->CreateFCmpONE(
        CondV,
        ConstantFP::get(*TheContext, APFloat(0.0)),
        "ifcond"
    );

    Function* TheFunction = Builder->GetInsertBlock()->getParent();

    // Create blocks for the then and else cases.
    // Insert the 'then' block at the function.
    BasicBlock* ThenBB = BasicBlock::Create(*TheContext, "then", TheFunction);
    BasicBlock* ElseBB = BasicBlock::Create(*TheContext, "else");
    BasicBlock* MergeBB = BasicBlock::Create(*TheContext, "ifcont");

    Builder->CreateCondBr(CondV, ThenBB, ElseBB);
    // Emit then Value
    Builder->SetInsertPoint(ThenBB);
    Value* ThenV = Then->codegen();
    if (!ThenV)
        return nullptr;
    Builder->CreateBr(MergeBB);
    // Codegen of 'Then' can change the current block, updates ThenBB for the PHI.
    ThenBB = Builder->GetInsertBlock();

    // Emit else block
    TheFunction->getBasicBlockList().push_back(ElseBB);
    Builder->SetInsertPoint(ElseBB);
    Value* ElseV = Else->codegen();
    if (!ElseV)
        return nullptr;
    Builder->CreateBr(MergeBB);
    // Codegen of 'Else' can change the current block, update ElseBB for the PHI.
    ElseBB = Builder->GetInsertBlock();

    // Emit merge block
    TheFunction->getBasicBlockList().push_back(MergeBB);
    Builder->SetInsertPoint(MergeBB);
    PHINode* PN = Builder->CreatePHI(Type::getDoubleTy(*TheContext), 2, "iftmp");

    PN->addIncoming(ThenV, ThenBB);
    PN->addIncoming(ElseV, ElseBB);

    return PN;
}

Value* ForExprAST::codegen() {
    auto TheFunction = Builder->GetInsertBlock()->getParent();
    // Create an alloca for the variable in the entry block.
    auto Alloca = CreateEntryBlockAlloca(TheFunction, VarName);

    KSDbgInfo.emitLoc(this);

    // Emit the start code first, without 'variable' in scope.
    Value* StartVal = Start->codegen();
    if (!StartVal)
        return nullptr;

    // Store the value into the alloca.
    Builder->CreateStore(StartVal, Alloca);

    // Make the new basic block for the loop header,
    // inserting after current block.
    auto LoopBB = BasicBlock::Create(*TheContext, "loop", TheFunction);

    // Insert an explicit fall through from the current block to the LoopBB.
    Builder->CreateBr(LoopBB);

    // Start insertion in LoopBB.
    Builder->SetInsertPoint(LoopBB);

    // Within the loop, the variable is defined equal to the PHI node.
    // If it shadows an existing variable, we have to restore it, so save it now.
    auto OldVal = NamedValues[VarName];
    NamedValues[VarName] = Alloca;

    // Emit the body of the loop.
    // This, like any other expr, can change the current BB.
    // Note that we ignore the value computed by the body, but don't allow an error.
    if (!Body->codegen())
        return nullptr;

    // Emit the step value.
    Value* StepVal = nullptr;
    if (Step) {
        StepVal = Step->codegen();
        if (!StepVal)
            return nullptr;
    } else {
        // If not specified, use 1.0
        StepVal = ConstantFP::get(*TheContext, APFloat(1.0));
    }

    // Compute the end condition.
    Value* EndCond = End->codegen();
    if (!EndCond)
        return nullptr;

    // Reload, increment, and restore gthe alloca.
    // This handles the case where the body of the loop mutates the variable/
    Value* CurVar = Builder->CreateLoad(Alloca, VarName.c_str());
    auto NextVar = Builder->CreateFAdd(CurVar, StepVal, "nextvar");
    Builder->CreateStore(NextVar, Alloca);

    // Convert condition to a bool by comparing non-equal to 0.0.
    EndCond = Builder->CreateFCmpONE(
        EndCond,
        ConstantFP::get(*TheContext, APFloat(0.0)),
        "loopcond"
    );

    // Create the "after loop" block and insert it.
    auto AfterBB = BasicBlock::Create(*TheContext, "afterloop", TheFunction);

    // Insertion the conditional branch into the end of LoopEndBB.
    Builder->CreateCondBr(EndCond, LoopBB, AfterBB);

    // Any new code will be inserted in AfterBB.
    Builder->SetInsertPoint(AfterBB);

    // Restore the unshadowed variable.
    if (OldVal)
        NamedValues[VarName] = OldVal;
    else
        NamedValues.erase(VarName);

    // for expr always returns 0.0.
    return Constant::getNullValue(Type::getDoubleTy(*TheContext));

}

Value* UnaryExprAST::codegen() {
    auto OperandV = Operand->codegen();
    if (!OperandV)
        return nullptr;

    auto F = getFunction(string("unary") + Opcode);
    if (!F)
        return LogErrorV("Unknown unary operator");

    KSDbgInfo.emitLoc(this);

    return Builder->CreateCall(F, OperandV, "unop");
}

Value* LetExprAST::codegen() {
    vector<AllocaInst*> OldBindings;

    auto TheFunction = Builder->GetInsertBlock()->getParent();

    // Register all variable and emit their initializer.
    for (unsigned i = 0, e = VarNames.size(); i != e; ++i) {
        const string &VarName = VarNames[i].first;
        ExprAST* Init = VarNames[i].second.get();

        // Emit the initializer before adding the variable to scope,
        // this prevents the initializer from referencing the variable itself,
        // and permits stuff like this:
        //   var a = 1 in
        //     var a = a in ...    #refers to outer 'a'.
        Value* InitVal;
        if (Init) {
            InitVal = Init->codegen();
            if (!InitVal)
                return nullptr;
        } else {  // if not specified, use 0.0
            InitVal = ConstantFP::get(*TheContext, APFloat(0.0));
        }

        auto Alloca = CreateEntryBlockAlloca(TheFunction, VarName);
        Builder->CreateStore(InitVal, Alloca);

        // Remember the old variable binding,
        // so that we can restore the binding when we unrecurse.
        OldBindings.push_back(NamedValues[VarName]);

        // Remember this binding.
        NamedValues[VarName] = Alloca;
    }

    KSDbgInfo.emitLoc(this);

    // Codegen the body, now that all vars are in scope.
    auto BodyVal = Body->codegen();
    if (!BodyVal)
        return nullptr;

    for (unsigned i = 0, e = VarNames.size(); i != e; ++i)
        NamedValues[VarNames[i].first] = OldBindings[i];

    return BodyVal;
}

#endif