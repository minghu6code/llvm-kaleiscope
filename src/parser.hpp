#ifndef PARSER_HPP
#define PARSER_HPP

#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include <string>
#include <cwchar>
#include <cctype>
#include <iostream>
#include <memory>
#include <utility>
#include <vector>
#include <map>

using llvm::Value;
using llvm::LLVMContext;
using llvm::IRBuilder;
using llvm::Module;
using llvm::ConstantFP;
using llvm::APFloat;
using llvm::Type;
using llvm::Function;
using llvm::FunctionType;
using llvm::BasicBlock;
using llvm::verifyFunction;
using llvm::errs;


using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::unique_ptr;
using std::make_unique;
using std::move;
using std::vector;
using std::map;
using std::pair;
using std::make_pair;

#include "utils.hpp"
#include "lexer.hpp"
#include "globals.hpp"

//===---------------------------------------------------------------------------------------
// Parser
//===---------------------------------------------------------------------------------------

/// CurTok/getNextToken - Provide a simple token buffer.
/// Curtok is the current token the parser is looking at.
/// getNextToken reads another token from the lexer and upadets CurTok with its results.
static int CurTok;
static int getNextToken() {
    return CurTok = gettok();
}

/// LogError* - There are little helper functions for error handling.
unique_ptr<ExprAST> LogError(const char* Str) {
    cerr << "LogError: " << Str << endl;
    return nullptr;
}

// is different with LogError from return type
// ExprASt vs PrototypeAST
unique_ptr<PrototypeAST> LogErrorP(const char* Str) {
    LogError(Str);
    return nullptr;
}

/// numberexpr ::= number
static unique_ptr<ExprAST> ParseNumberExpr() {
    auto Result = make_unique<NumberExprAST>(NumVal);
    getNextToken();
    return move(Result);
}

// Just to resolved the stupid compiler frontend can't recognize circular dependency of definition.
static unique_ptr<ExprAST> ParseExpression();

/// identifierexpr
///     ::= identifier, such as 'var1' - Type A
///     ::= identifier '(' expression* ')', such as 'F(var1, var2, var3)' - Type B
static unique_ptr<ExprAST> ParseIdentifierExpr() {
    string IdName = IdentifierStr;
    SrcLoc IdLoc = CurLoc;

    getNextToken();

    if (CurTok != '(')  // Type A
        return make_unique<VariableExprAST>(IdLoc, IdName);

    // Type B
    getNextToken();
    vector<unique_ptr<ExprAST>> Args;
    if (CurTok != ')') {
        while (1) {
            if (auto Arg = ParseExpression())  // Top Level recusive call
                Args.push_back(move(Arg));
            else
                return nullptr;

            if (CurTok == ')')
                break;

            // if (CurTok != ',')
            //     return LogError("Expected ')' or ',' in argument list");

            // getNextToken();
        }
    }

    getNextToken();

    return make_unique<CallExprAST>(IdLoc, IdName, move(Args));
}

static unique_ptr<ExprAST> ParseParenExpr() {
    getNextToken();  // eat '('
    auto V = ParseExpression();  // parse InnerExpression
    if (!V)
        return nullptr;

    if (CurTok != ')')
        return LogError("expected ')'");
    getNextToken();  // eat ')'
    return V;
}

/// ifexpr ::= if<white>+<expr><white>+else<white>+<expr>
static unique_ptr<ExprAST> ParseIfExpr() {
    getNextToken();  // eat if
    SrcLoc IfLoc = CurLoc;

    auto Cond = ParseExpression();
    if (!Cond)
        return nullptr;

    auto Then = ParseExpression();
    if (!Then)
        return nullptr;

    unique_ptr<ExprAST> Else;
    if (CurTok != tok_else)
        Else = make_unique<NumberExprAST>(0);  // default 0.0
    else {
        getNextToken();
        Else = ParseExpression();
        if (!Else)
            return nullptr;
    }

    return make_unique<IfExprAST>(IfLoc, move(Cond), move(Then), move(Else));
}

/// forexpr ::= 'for'
/// extern putchard(char);
/// for i = 0; i < 10; 2;
///     putchard(42);
static unique_ptr<ExprAST> ParseForExpr() {
    getNextToken();  // eat the for.

    if (CurTok != tok_identifier)
        return LogError("expected identifier after for");

    string IdName = IdentifierStr;
    getNextToken();

    if (CurTok != '=')
        return LogError("expected '=' after for");
    getNextToken();

    auto Start = ParseExpression();
    if (!Start)
        return nullptr;
    if (CurTok != ';')
        return LogError("expected ';' after for start value");
    getNextToken();

    auto End = ParseExpression();
    if (!End)
        return nullptr;

    // The step value is optional.
    getNextToken();
    unique_ptr<ExprAST> Step = ParseExpression();
    if (!Step)
        return nullptr;

    if (CurTok != ';')
        return LogError("expected ';' after for");
    getNextToken();

    auto Body = ParseExpression();
    if (!Body)
        return nullptr;

    return make_unique<ForExprAST>(
        IdName,
        move(Start),
        move(End),
        move(Step),
        move(Body)
    );
}

static unique_ptr<ExprAST> ParseLetExpr() {
    getNextToken();

    vector<pair<string, unique_ptr<ExprAST>>> VarNames;

    // At least one variable name is required.
    if (CurTok != tok_identifier)
        return LogError("expected identifier after var");

    while (1) {
        string Name = IdentifierStr;
        getNextToken();

        unique_ptr<ExprAST> Init;
        if (CurTok == '=') {
            getNextToken();

            Init = ParseExpression();
            if (!Init)
                return nullptr;
        }

        VarNames.push_back(make_pair(Name, move(Init)));

        // End of var list, exit loop.
        if (CurTok != ',')
            break;
        getNextToken();

        if (CurTok != tok_identifier)
            return LogError("expected identifier list after var");
    }
    // // At this point, we have to have 'in'.
    // if (CurTok != tok_in)
    //     return LogError("expected 'in' keyword after 'var'");
    // getNextToken();

    auto Body = ParseExpression();
    if (!Body)
        return nullptr;

    return make_unique<LetExprAST>(move(VarNames), move(Body));
}

/// primary
///     ::= identifier
///     ::= numberexpr
///     ::= parenexpr
static unique_ptr<ExprAST> ParsePrimary() {
    switch (CurTok) {
    default:
        return LogError("unknown token when expecting an expression");
    case tok_identifier:
        return ParseIdentifierExpr();
    case tok_number:
        return ParseNumberExpr();
    case '(':
        return ParseParenExpr();
    case tok_if:
        return ParseIfExpr();
    case tok_for:
        return ParseForExpr();
    case tok_let:
        return ParseLetExpr();
    }
}

/// This holds the precedence(AKA priority) for each binary operator
static map<char, int> BinopPrecedence = {
    {'<', 10},
    {'+', 20},
    {'-', 20},
    {'*', 40},
    {'=', 2},
};

/// Get the precedence of pending binary operator token.
static int GetTokPrecedence() {
    if (!isascii(CurTok))
        return -1;

    int TokPrec = BinopPrecedence[CurTok];
    if (TokPrec <= 0) return -1;
    return TokPrec;
}

static unique_ptr<ExprAST> ParseUnary() {
    if (!isascii(CurTok) || CurTok == '(' || CurTok == ',')
        return ParsePrimary();

    // If this is a unary operator, read it.
    int Opcode = CurTok;
    getNextToken();
    if (auto Operand = ParseUnary())
        return make_unique<UnaryExprAST>(Opcode, move(Operand));
    return nullptr;
}

static unique_ptr<ExprAST> ParseBinOpRHS(int ExprPrec, unique_ptr<ExprAST> LHS) {
    while (1) {
        int TokPrec = GetTokPrecedence();

        if (TokPrec < ExprPrec)  // return if token isn't binop
            return LHS;

        int BinOp = CurTok;
        SrcLoc BinLoc = CurLoc;
        getNextToken();

        auto RHS = ParseUnary();
        if (!RHS)  // LogError returns nullptr
            return nullptr;
        // new binop precedence compared with old binop
        if (TokPrec < GetTokPrecedence()) {  // a oldbinop (b newbinop unparsed)
            RHS = ParseBinOpRHS(TokPrec + 1, move(RHS));
            if (!RHS)
                return nullptr;
        }

        // Merge LHS/RHS
        LHS = make_unique<BinaryExprAST>(BinLoc, BinOp, move(LHS), move(RHS));  // (a oldbinop b)
    }
}


/// expression
///     ::= primary binoprhs
static unique_ptr<ExprAST> ParseExpression() {
    auto LHS = ParseUnary();
    if (!LHS)
        return nullptr;

    return ParseBinOpRHS(0, move(LHS));
}


/// prototype
///     ::= function prototype of declaration/definition extern f(a b c) or f(a b c)
static unique_ptr<PrototypeAST> ParsePrototype() {
    string FnName;

    SrcLoc FnLoc = CurLoc;

    unsigned Kind = 0;  // 0 indetifier, 1 unary, 2 binary
    unsigned BinaryPrecedence = 30;

    switch (CurTok) {
    default:
        return LogErrorP("Expected function name in prototype");
    case tok_identifier:
        FnName = IdentifierStr;
        Kind = 0;
        getNextToken();
        break;
    case tok_binary:
        getNextToken();
        if (!isascii(CurTok))
            return LogErrorP("Expected binary operator");
        FnName = "binary";
        FnName += (char)CurTok;
        Kind = 2;
        getNextToken();

        // Read the precedence if present.
        if (CurTok == tok_number) {
            if (NumVal < 1 || NumVal > 100)
                return LogErrorP("Invalid precedence: must be 1..100");
            BinaryPrecedence = (unsigned)NumVal;
            getNextToken();
        }
        break;
    case tok_unary:
        getNextToken();
        if (!isascii(CurTok))
            return LogErrorP("Expected unary operator");
        FnName = "unary";
        FnName += (char)CurTok;
        Kind = 1;
        getNextToken();
        break;
    }

    if (CurTok != '(')
        return LogErrorP("Expected '(' in prototype");

    vector<string> ArgNames;
    while (getNextToken() == tok_identifier)
        ArgNames.push_back(IdentifierStr);
    if (CurTok != ')')
        return LogErrorP(string_format("Expected ')', but %d found in prototype", CurTok).c_str());

    getNextToken();

    if (Kind && ArgNames.size() != Kind)
        return LogErrorP("Invalid number of operands for operator");

    return make_unique<PrototypeAST>(FnLoc, FnName, move(ArgNames), Kind != 0, BinaryPrecedence);
}

/// definition ::= 'def' prototype expressison
static unique_ptr<FunctionAST> ParseDefinition() {
    getNextToken();  // eat def.
    auto Proto = ParsePrototype();
    if (!Proto)
        return nullptr;

    if (auto E = ParseExpression())
        return make_unique<FunctionAST>(move(Proto), move(E));
    return nullptr;
}

/// external ::= 'extern' prototype
static unique_ptr<PrototypeAST> ParseExtern() {
    getNextToken();  // eat extern.
    return ParsePrototype();
}

/// toplevelexpr ::= expression
static unique_ptr<FunctionAST> ParseTopLevelExpr() {
    SrcLoc FnLoc = CurLoc;

    if (auto E = ParseExpression()) {
        // Make an anonymous proto.
        auto Proto = make_unique<PrototypeAST>(FnLoc, "__anon_expr", vector<string>());  // anonymous zero arguments function
        return make_unique<FunctionAST>(move(Proto), move(E));
    }
    return nullptr;
}

#endif