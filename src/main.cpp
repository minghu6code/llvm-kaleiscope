#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include "llvm/IR/Metadata.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Transforms/InstCombine/InstCombine.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Scalar/GVN.h"
#include "llvm/Transforms/Utils.h"

#include <string>
#include <cwchar>
#include <cctype>
#include <iostream>
#include <memory>
#include <utility>
#include <vector>
#include <map>

#include "utils.hpp"
#include "ast.hpp"
#include "parser.hpp"
#include "codegen.hpp"
#include "library.hpp"

using llvm::Value;
using llvm::LLVMContext;
using llvm::IRBuilder;
using llvm::Module;
using llvm::ConstantFP;
using llvm::APFloat;
using llvm::Type;
using llvm::Function;
using llvm::FunctionType;
using llvm::BasicBlock;
using llvm::verifyFunction;
using llvm::errs;
using llvm::outs;
using llvm::createPromoteMemoryToRegisterPass;
using llvm::createInstructionCombiningPass;
using llvm::createReassociatePass;
using llvm::createGVNPass;
using llvm::createCFGSimplificationPass;
using llvm::InitializeNativeTarget;
using llvm::InitializeNativeTargetAsmPrinter;
using llvm::InitializeNativeTargetAsmParser;
using llvm::InitializeAllTargetInfos;
using llvm::InitializeAllTargets;
using llvm::InitializeAllTargetMCs;
using llvm::InitializeAllAsmParsers;
using llvm::InitializeAllAsmPrinters;
using llvm::TargetRegistry;
using llvm::TargetOptions;
using llvm::Optional;
using llvm::raw_fd_ostream;
using llvm::CGFT_ObjectFile;

using llvm::Reloc::Model;

using llvm::orc::ThreadSafeModule;

using llvm::sys::getDefaultTargetTriple;
using llvm::sys::fs::OF_None;
using llvm::sys::getProcessTriple;
//ns llvm::legacy; FunctionPassManager
using llvm::DEBUG_METADATA_VERSION;
using llvm::Triple;
using llvm::dwarf::DW_LANG_C;

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::unique_ptr;
using std::make_unique;
using std::move;
using std::vector;
using std::map;
using std::error_code;


//===---------------------------------------------------------------------------------------
// Top-Level parsing and JIT Driver
//===---------------------------------------------------------------------------------------
void InitializeModule() {
    // Open a new context and module.
    TheContext = make_unique<LLVMContext>();
    TheModule = make_unique<Module>("my cool jit", *TheContext);
    TheModule->setDataLayout(TheJIT->getDataLayout());

    // Create a new builder for the module.
    Builder = make_unique<IRBuilder<>>(*TheContext);
}

static void HandleDefinition() {
    if (auto FnAST = ParseDefinition()) {
        if (!FnAST->codegen()) {
            cerr << "Error reading function definition:";
        }
    } else {
        getNextToken();  // Skip token for error recorvery.
    }
}

static void HandleExtern() {
    if (auto ProtoAST = ParseExtern()) {
        if (!ProtoAST->codegen())
            cerr << "Error reading extern";
        else
            FunctionProtos[ProtoAST->getName()] = move(ProtoAST);
    } else {
        getNextToken();
    }
}

static void HandleTopLevelExpression() {
    // Evaluate a top-level expression into an anonymous function.
    if (auto FnAST = ParseTopLevelExpr()) {
        if (!FnAST->codegen())
            cerr << "Error generating code for top level expr";
    } else {
        // Skip token fro error recovery.
        getNextToken();
    }
}

/// top ::= definition | external | expression | ;
static void MainLoop() {
    while (true) {
        switch (CurTok) {
        case tok_eof:
            return;
        case ';':  // ignore this token
            getNextToken();
            break;
        case tok_def:
            HandleDefinition();
            break;
        case tok_extern:
            HandleExtern();
            break;
        default:
            HandleTopLevelExpression();
            break;
        }
    }
}

//===---------------------------------------------------------------------------------------
// Main driver code.
//===---------------------------------------------------------------------------------------
int main() {
    InitializeNativeTarget();
    InitializeNativeTargetAsmPrinter();
    InitializeNativeTargetAsmParser();

    getNextToken();

    TheJIT = ExitOnErr(KaleidoscopeJIT::Create());

    InitializeModule();

    // Add the current debug info version into the module.
    TheModule->addModuleFlag(
        Module::Warning,
        "Debug Info Version",
        DEBUG_METADATA_VERSION
    );

    // Darwin only supports dwarf2.
    if (Triple(getProcessTriple()).isOSDarwin())
        TheModule->addModuleFlag(Module::Warning, "Dwarf Version", 2);

    // Construct the DIBuilder, we do this here because we need the module.
    DBuilder = make_unique<DIBuilder>(*TheModule);

    // Create the compile unit for the module.
    // Currently down as "fib.ks" as a filename since we're redirecting stdin
    // but we'd like actual source locations.
    KSDbgInfo.TheCU = DBuilder->createCompileUnit(
        DW_LANG_C, DBuilder->createFile("fib.ks", "."),
        "Kaleidoscope Compiler", 0, "", 0);

    // Run the main "interrupt loop" now.
    MainLoop();

    // Finalize the debug info.
    DBuilder->finalize();

    // Print out all of the generated code.
    TheModule->print(errs(), nullptr);

    return 0;
}