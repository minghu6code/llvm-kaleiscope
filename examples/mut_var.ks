# Define ':' for sequencing: as a low-precedence operator that ignores operands
# and just returns the RHS.
def binary : 1 (x y) y;

# Recursive fib, we could do this before.
def fib(x)
  if (x < 3)
    1
  else
    fib(x-1)+fib(x-2);

# Iterative fib.
def fibi(x)
  let a = 1, b = 1, c
  (for i = 3; i < x; 1;
     c = a + b :
     a = b :
     b = c) :
  b;

# Call it.
fibi(10);